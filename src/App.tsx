import React, { useState, useEffect } from 'react';
import styled, { keyframes, css } from 'styled-components';

interface PlayerInterface {
  userID: string;
  displayName: string;
  picture: string;
  score: number;
}

const initialPlayers: PlayerInterface[] = [
  { userID: 'u-1', displayName: 'Jone', picture: '', score: 30000 },
  { userID: 'u-2', displayName: 'Victoria', picture: '', score: 29500 },
  { userID: 'u-3', displayName: 'Joy', picture: '', score: 29200 },
  { userID: 'u-4', displayName: 'Quinn', picture: '', score: 29000 },
  { userID: 'u-5', displayName: 'Sheenalo', picture: '', score: 28900 },
  { userID: 'u-6', displayName: 'Charlene', picture: '', score: 28800 },
  { userID: 'u-7', displayName: 'LeonaBaby', picture: '', score: 28500 },
  { userID: 'u-8', displayName: 'Sunny', picture: '', score: 28300 },
  { userID: 'u-9', displayName: 'ImWord', picture: '', score: 28100 },
  { userID: 'u-10', displayName: 'Dophine', picture: '', score: 28000 },
];

const App: React.FC = () => {
  const [players, setPlayers] = useState<PlayerInterface[]>(initialPlayers);
  const [prevOrder, setPrevOrder] = useState<string[]>(initialPlayers.map(p => p.userID));

  useEffect(() => {
    const interval = setInterval(() => {
      setPlayers(prevPlayers => {
        const updatedPlayers = prevPlayers.map(player => ({
          ...player,
          score: player.score + Math.floor(Math.random() * 100),
        }));

        updatedPlayers.sort((a, b) => b.score - a.score);

        const newOrder = updatedPlayers.map(p => p.userID);
        const orderChanged = !newOrder.every((id, index) => id === prevOrder[index]);

        if (orderChanged) {
          setPrevOrder(newOrder);
        }

        return updatedPlayers;
      });
    }, 1000);

    return () => clearInterval(interval);
  }, [prevOrder]);

  return (
    <CenteredContainer>
      <Leaderboard>
        {players.map((player, index) => (
          <Player key={player.userID} move={player.userID !== prevOrder[index]}>
            <SerialNumber rank={index + 1}>{index + 1}</SerialNumber>
            <ProfileImage src={player.picture || 'https://via.placeholder.com/30'} alt={player.displayName} />
            <PlayerName>{player.displayName}</PlayerName>
            <Score>{player.score}pt</Score>
          </Player>
        ))}
      </Leaderboard>
    </CenteredContainer>
  );
};

export default App;

const updateScore = keyframes`
  from { opacity: 0; transform: scale(0.5); }
  to { opacity: 1; transform: scale(1); }
`;

const movePlayer = keyframes`
  from { transform: translateY(0); }
  to { transform: translateY(-10px); }
`;

const CenteredContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const Leaderboard = styled.div`
  width: 800px;
  background-color: #95bfea;; /* Dark blueish */
  padding: 20px;
  border-radius: 10px;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
`;

const Player = styled.div<{ move: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 10px 0;
  padding: 10px;
  border-radius: 5px;
  background-color: #ffffff; /* White */
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  ${({ move }) => move && css`
    animation: ${movePlayer} 0.5s, ${updateScore} 0.5s;
  `}
`;

const Score = styled.span`
  font-weight: bold;
  font-size: 1.2rem;
`;

const SerialNumber = styled.span<{ rank: number }>`
  display: inline-block;
  width: 30px;
  height: 30px;
  line-height: 30px;
  border-radius: 50%;
  background-color: ${({ rank }) => rank === 1 ? 'red' : rank === 2 ? 'orange' : rank === 3 ? 'yellowgreen' : '#007bff'};
  color: white;
  text-align: center;
  margin-right: 10px;
`;

const ProfileImage = styled.img`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: silver;
  margin-right: 10px;
`;

const PlayerName = styled.span`
  flex-grow: 1;
`;
